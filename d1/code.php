<?php

//comment
/*sasdasdadas sasdasdadas
sdfsd*/

//variables
$name = 'John smith';
$email = 'asdasf@yahoo.com';

//constants
define('PI', 3.1416);

//data types

//strings

$state = 'New york';
$country = 'United States of America';
$address = $state. ', '.$country; //concatenenate
$address2 = "$state, $country";

//integers
$age = 31;
$headcount = 26;

//floats

$grade = 98.2;
$distanceInKm = 2562.24;

//boolean 
$hasTravelledAbroad = false;
$haveSymptoms = true;

//null 
$spouse = null;

//array
$grade = array(44.34, 34324, 342, 43.5);

//objects
$gradeObject = (object)[
	'firstGrading' => 33,
	'2nd' => 223
];
$gradeObject = (object)[
	'firstGrading' => 33,
	'2nd' => 223,
	'address' => (object)[
		'state' => 'washington',
		'country' => 'US'
	]
];

$x = 56.2;
$y = 2423423;

$isLegalAge = true;
$isRegistered = false;

function getFullName($firstName, $middleInitial, $lastName){
	return "$lastName, $firstName $middleInitial";
}

function typhoon($windSpeed){
	if($windSpeed < 30){
		return "not typhoon";
	} else if ($windSpeed <= 61) {
		return "depression";
	} else if ($windSpeed >= 61 && $windSpeed <= 88) {
		return "storm";
	} else {
		return "invalid";
	}

}

//conditional (ternary) operator

function isUnderAge($age){
	return ($age < 18) ? true : false;
}

//switch

function test($test){
	switch ($test) {
		case 1:
			return 'test1';
			break;
		case 2:
			return 'test2';
			break;
		default:
			return 'test0';
	}
}

function greeting($str){
	try{
		if(gettype($str) == "string"){
			echo $str;
		} else{
			throw new Exception("Opps!");
		}
	} 
	catch(Exception $e){
		echo $e->getMessage();
	}
	finally{
		echo "I did it again!";
	}
}