<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h1>Echoing Values</h1>
	<!-- single quotes - variables cant be used, instead use doouble quotes -->
	<p><?php echo 'Hello BATCH 168! :)'?></p>
	<p><?php echo "name $name! email is $email.";?></p>
	<p><?php echo PI; ?></p>
	<p><?php echo $address; ?></p>
	<p><?php echo $address2; ?></p>
	<p><?php echo $hasTravelledAbroad; ?></p>
	<p><?php echo $spouse; ?></p>
	<p><?php echo $haveSymptoms; ?></p>
	<p><?php echo gettype($haveSymptoms); ?></p>
	<p><?php echo var_dump($haveSymptoms); ?></p>
	<p><?php echo $gradeObject->firstGrading; ?></p>
	<p><?php echo $grade[0]; ?></p>
	<p><?php echo $gradeObject->address->state; ?></p>

	<h1>Operators</h1>
	<p>X: <?php echo $x; ?></p>
	<p>Y: <?php echo $y; ?></p>
	<p>is legal age: <?php echo var_dump($isLegalAge); ?></p>
	<p>is registered: <?php echo var_dump($isRegistered); ?></p>
	<p>is registered: <?php echo var_dump($isRegistered); ?></p>
	<p>Loose Equality: <?php echo var_dump($x == '56.2') ?></p>
	<p>Strict Equality: <?php echo var_dump($x === '56.2') ?></p>
	<p>Loose Equality: <?php echo var_dump($x != '56.2') ?></p>
	<p>Strict Equality: <?php echo var_dump($x !== '56.2') ?></p>

	<h2>Greater/Lesser Operators</h2>

	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Lesser: <?php echo var_dump($x > $y); ?></p>

	<h2>Logical Operators</h2>
	<p>Are all requirements met: <?php echo var_dump($isLegalAge && $isRegistered) ?></p>
	<p>Are some requirements met: <?php echo var_dump($isLegalAge || $isRegistered) ?></p>
	<p>Are some requirements not met: <?php echo var_dump(!$isLegalAge && !$isRegistered) ?></p>
	<h1>Functions</h1>
	<p>Full Name: <?php echo getFullName('adsda', 'a', 'hghfg') ?></p>

	<h1>Slection Control Structures</h1>
	<h2>if-elseif-else</h2>
	<p><?php echo typhoon(40) ?></p>
	<h1>ternary</h1>
	<p>78: <?php echo var_dump(isUnderAge(78)) ?></p>
	<p>7: <?php echo var_dump(isUnderAge(7)) ?></p>
	<h1>switch</h1>
	<p><?php echo test(2) ?></p>

	<h1>try catch finally</h1>
	<p><?php echo greeting("12"); ?></p>
</body>
</html>